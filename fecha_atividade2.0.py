import os
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException, ElementNotVisibleException, WebDriverException


def menu_principal():
    try:
        gg.implicitly_wait(5)
        element = WebDriverWait(gg, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[5]/div[3]')))
        element.click()
        return(True)
    except (TimeoutException, WebDriverException):
        print('Erro ao clicar na guia de atividade - demorou demais.')
        gg.refresh()
        return menu_principal()


def insere_texto(txt_resolucao):
        gg.implicitly_wait(10)
        try:
            area = WebDriverWait(gg, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[7]/div[3]/div/div[1]/div[5]/textarea')))
            area.send_keys(txt_resolucao)
            return print("Texto de resolução inserido")
        except TimeoutException:
            print('Area para inserir a resolção não encontrada, atualizando.')
            gg.refresh()            
            return insere_texto(txt_resolucao)


def concluir_atividade():
        gg.implicitly_wait(5)
        try:
            btn_concluir = WebDriverWait(gg, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[7]/div[3]/div/div[1]/div[6]/input[1]')))
            btn_concluir.click()
            gg.implicitly_wait(3)
            return print("Atividade concluida")
        except (NoSuchElementException, ElementNotVisibleException, WebDriverException):
            gg.refresh()


def fim():
    print("\n\n")
    print(' Acabou de fechar todas as atividades')
    gg.quit()
    print("\n\n")
    os.system("pause")


# Main

while True:
    qnt_atividades  = " "
    txt_resolucao   = " "

    while not (qnt_atividades.isdigit() and txt_resolucao.strip()):
        os.system('cls')
        print('\n')

        qnt_atividades  = input('   Quantas atividades serão fechadas? ')
        txt_resolucao   = str(input('   Qual o texto de resolução padrão? '))

    qnt_atividades = int(qnt_atividades)

    gg = webdriver.Chrome(".\\chromedriver.exe")
    gg.get('https://cstiprod.sesisenaisp.org.br/MyActivities')

    cond = menu_principal()
    if cond is True:
        while(qnt_atividades > 0):
            print("Fechando a atividade {}".format(qnt_atividades))
            insere_texto(txt_resolucao)
            concluir_atividade()
            qnt_atividades -= 1
            print("Atividade fechada")
            print("")
            gg.refresh()
        else:
            fim()
    else:
        print("Deu algum erro")
